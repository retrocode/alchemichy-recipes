<?php

namespace Recipes;

class Input {
    /** @var CLImate */
    private $commandLine;

    public function __construct() {
        $this->commandLine = new \League\CLImate\CLImate;
    }

    public function askForItemFromOptions(array $options, string $question): string
    {
        $input = $this->commandLine->radio($question, $options);

        return $input->prompt();
    }
}

