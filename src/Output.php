<?php

namespace Recipes;

class Output {
    /** @var CLImate */
    private $commandLine;

    public function __construct() {
        $this->commandLine = new \League\CLImate\CLImate;
    }

    public function info(string $message): void
    {
        $this->commandLine->info($message);
    }
}
