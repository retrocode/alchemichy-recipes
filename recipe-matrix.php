<?php

require_once('vendor/autoload.php');

$output = new Recipes\Output();
$input  = new Recipes\Input();

$foodItems      = ['Chicken', 'Pork' ];
$cookingMethods = ['Raw', 'Fried' ];

$chosenFoodItem      = $input->askForItemFromOptions($foodItems, 'Choose a food item: ');
$chosenCookingMethod = $input->askForItemFromOptions($cookingMethods, 'Choose a method of cooking: ');

$output->info("You have chosen '$chosenFoodItem' using cooking method '$chosenCookingMethod'");

